const jwt = require('jsonwebtoken');
const User = require('../models/user.model');
const SECRET_KEY = require('../config/auth.config');

const jwtValidator = async (req, res, next) => {
  try {
    const auth = req.headers.authorization
    if (!auth) {
      throw new Error(400, 'No token provided!');
    }
    const token = auth.split(' ')[1]
    const verified = jwt.verify(token, SECRET_KEY)
    const currentUser = await User.findById(verified['_id'])

    if (!currentUser || currentUser.token !== token) {
      throw new Error(400, 'Unauthorized! Invalid JWT token');
    }

    req.verifiedUser = verified
    next()
  } catch (error) {
    res.status(error.status || 400).json({message: error.message})
  }
}

module.exports = jwtValidator