const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
require('dotenv').config();
require('./config/db.config');

const DEFAULT_PORT = 8080;
const PORT = process.env.PORT || DEFAULT_PORT;

const authRouter = require('./routes/api/auth.routes')
const usersRouter = require('./routes/api/users.routes')
const notesRouter = require('./routes/api/notes.routes')

const app = express();

app.use(morgan('combined'));
app.use(cors());
app.use(express.json());

app.use('/api/auth', authRouter)
app.use('/api/users', usersRouter)
app.use('/api/notes', notesRouter)

app.use((req, res) => {
  res.status(404).json({ message: 'Not found' })
})

app.use((err, req, res, next) => {
  res.status(500).json({ message: err.message })
})



app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`)
})