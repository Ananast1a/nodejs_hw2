const { Router } = require('express')

const {createNote, getNotes, getNote, deleteNote, toggleCheckNote, changeNote} = require('../../controllers/notes.controller')
const jwtValidator = require('../../middleware/jwtValidator')

const router = Router()

router.use('/', jwtValidator)
router.post('/', createNote)
router.get('/', getNotes)
router.get('/:id', getNote)
router.delete('/:id', deleteNote)
router.patch('/:id', toggleCheckNote)
router.put('/:id', changeNote)

module.exports = router