const { Router } = require('express')

const {getUserInfo, deleteUser, changePassword} = require('../../controllers/users.controller')
const jwtValidator = require('../../middleware/jwtValidator')

const router = Router()

router.use('/me', jwtValidator)
router.get('/me', getUserInfo)
router.delete('/me', deleteUser)
router.patch('/me', changePassword)


module.exports = router