const bcrypt = require('bcryptjs');
const rounds = 7;


const hashPassword = async (password) => {
  const hashed = await bcrypt.hash(password, rounds);
  return hashed;
}

const validateHashedPassword = async (hashedPassword, password) => {
  const validation = await bcrypt.compare(password, hashedPassword)
  if (!validation) {
    throw new Error(400, 'Invalid password')
  }
}

module.exports = {
  hashPassword,
  validateHashedPassword
}
