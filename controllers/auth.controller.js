const jwt = require('jsonwebtoken');
const SECRET_KEY = require('../config/auth.config');
const User = require('../models/user.model');
const getCreatedDate = require('../services/getCreatedDate');
const {hashPassword, validateHashedPassword} = require('../services/bcryptPasswordService');


const registerUser = async (req, res) => {
  try {
    const createdDate = getCreatedDate();
    const {username, password} = req.body;
    const user = {
      username,
      createdDate,
      password: await hashPassword(password)
    }
    await User.create(user);
    res.status(200).json({
      message: 'Success'
    })
  } catch (error) {
    console.log(error);
    if (!error.status) {
      res.status(500).json({
        message: 'Internal server error'
      })
    } else {
      res.status(400).json({
        message: 'Bad request'
      })
    }

  }
}

const loginUser = async (req, res) => {
  try {
    
    const visitor = req.body;

    const user = await User.findOne({
      username: visitor.username
    })
    if (!user) {
      throw new Error(400, 'Invalid username')
    }
    await validateHashedPassword(user.password, visitor.password)
    const payload = {
      username: user.username,
      _id: user['_id']
    }
    user.token = jwt.sign(payload, SECRET_KEY)
    await user.save()
    res.status(200).json({
      message: 'Success',
      jwt_token: user.token
    })
  } catch (error) {
    if (!error.status) {
      res.status(500).json({
        message: 'Internal server error'
      })
    } else {
      res.status(400).json({
        message: 'Bad request'
      })
    }

  }
}

module.exports = {registerUser, loginUser}